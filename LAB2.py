#Declaramos Variable
nombre=""
cedula=0
new_nombre=0
#Declaramos Diccionario
datos_per=dict()

def registrar_datos():
    '''Procedimiento para registrar datos'''
    print("Registro de Datos")
    agregar='s'
    while agregar!= 'n':
        for i in agregar:
            cedula = int(input("Ingrese cedula: "))
            nombre = input("Ingrese Nombre: ")
            datos_per[cedula]=nombre
            mensaje = input("Desea agregar mas datos (s/n) : ")
            agregar = mensaje.lower()

    lineasSeparador()


def recorrer_datos():
    '''Procedimiento para recorrer datos'''
    print("Recorrer Datos")
    for i in datos_per:
        print ("Cedula:",i, " = ", "Nombre:",datos_per[i])
    lineasSeparador()

def eliminar_datos():
    '''Procedimiento para eliminar datos'''
    print("Eliminar Datos")
    cedula = int(input("Ingrese cedula: "))
    del datos_per[cedula]

    lineasSeparador()

def consultar_datos():
    '''Procedimiento para consultar datos'''
    print("Consultar Datos")
    consultar = 's'
    while consultar != 'n':
        for i in consultar:
            cedula = int(input("Ingrese cedula: "))
            print("Nombre:",datos_per.get(cedula))
            mensaje = input("Desea consultar mas datos (s/n) : ")
            consultar = mensaje.lower()

    lineasSeparador()
    menu_principal()


def cambiar_datos():
    '''Procedimiento para cambiar datos'''
    print("Cambiar Datos")
    cambiar = 's'
    while cambiar != 'n':
        for i in cambiar:
            cedula = int(input("Ingrese cedula: "))
            print("Nombre:", datos_per.get(cedula))

            new_nombre=input("Ingrese nuevo nombre: ")
            datos_per[cedula]=new_nombre
            print("Nuevo Nombre:", datos_per.get(cedula))
            #print("Cedula:",j, " = ", "Nombre:", datos_per[j])
            mensaje = input("Desea cambiar mas datos (s/n) : ")
            cambiar = mensaje.lower()
    lineasSeparador()
def documentacion():
    print(menu_principal.__doc__)
    print(registrar_datos.__doc__)
    print(recorrer_datos.__doc__)
    print(eliminar_datos.__doc__)
    print(consultar_datos.__doc__)
    print(cambiar_datos.__doc__)
    print(lineasSeparador.__doc__)

    lineasSeparador()


def lineasSeparador():
    '''Creacion de linea separadora'''
    print("==========================================")

def menu_principal():
    '''Creacion del menu del programa'''
    opcion = 0

    while (opcion!=7):
        opcion = int(input("Menu principal \n"
					   "1. Registrar Datos \n"
					   "2. Recorrer Datos \n"
					   "3. Eliminar Datos \n"
					   "4. Consultar Datos \n"
					   "5. Cambiar Datos \n"
                       "6. Ver Documentacion \n"
					   "7. Salir \n"
					   "Ingrese la opcion deseada : "))
        if opcion==1:
            registrar_datos()
        if opcion==2:
            recorrer_datos()
        if opcion==3:
            eliminar_datos()
        if opcion==4:
            consultar_datos()
        if opcion==5:
            cambiar_datos()
        if opcion==6:
            documentacion()
    else:
        print("Gracias por Utilizar el Programa")


menu_principal()
